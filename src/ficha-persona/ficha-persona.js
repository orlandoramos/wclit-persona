import { LitElement, html, css } from 'lit-element';
import { OrigenPersona } from '../origen-persona/origen-persona.js'
class FichaPersona extends LitElement {

    static get styles() {
        return css`
      div {
        border: 1px solid;
        border-radius: 10px;
        padding: 10px;
        margin: 10px;
      }
    `;
    }

    static get properties() {
        return {
            nombre: { type: String },
            apellidos: {
                type: String,
                hasChanged(newVal, oldVal) {
                    if (newVal != oldVal) {
                        console.log("se requiere input para que funcione ");
                        return true;
                    }
                    return false;
                }
            },
            anyosAntiguedad: { type: Number },
            nivel: { type: String },
            foto: { type: Object },
            bg: {type: String}

        };
    }

    updated(changedProperties) {
        if (changedProperties.has("nombre")) {

            console.log("nombre modificado: " + changedProperties.get("nombre") + " " + this.nombre);
        }
        if (changedProperties.has("apellidos")) {

            console.log("apellidos modificados: " + changedProperties.get("apellidos") + " " + this.apellidos);
        }

        if (changedProperties.has("anyosAntiguedad")) {

            this.actualizarNivel();

        }
    }

    actualizarNivel() {

        if (this.anyosAntiguedad >= "3")
            this.nivel = "junior";
        if (this.anyosAntiguedad >= "5")
            this.nivel = "senior";
        if (this.anyosAntiguedad >= "7")
            this.nivel = "lider";
        console.log(this.anyosAntiguedad + " nivel actualizados: " + this.nivel);
    }

    constructor() {
        super();
        this.nombre = "Pedro";
        this.apellidos = "Lopez Lopez";
        this.anyosAntiguedad = 4;
        this.nivel = "";
        this.foto = {
            src: "./img/persona.jpg",
            alt: "Pedro LL"
        }
        this.bg = "aliceblue";

    }

    render() {
        return html`

        <div style="width:200px; heigth:400px;background:${this.bg}">
            <label for="nombre">Nombre:</label>
            <input type="text" id="inombre" value = "${this.nombre}" @input="${this.updateNombre}"/>
            <br />
            <label for="apellidos">Apellidos:</label>
            <input type="text" id="iapellidos" value = "${this.apellidos}"  @input="${this.updateApellidos}/>
            <br />
            <label for="antiguedad">Antiguedad:</label>
            <input type="number" id="iantiguedad" value = "${this.anyosAntiguedad}" @input="${this.updateAnyosAntiguedad}"/>
            <br />
            <label for="nivel">Nivel:</label>
            <input type="text" id="inivel" value = "${this.nivel}" disabled/>
            <br />
            <origen-persona @origen-set="${this.origenChange}"></origen-persona>
            <img src="${this.foto.src}" height="200" width="200" alt="${this.foto.alt}"/>
        </div>
      
    `;
    }

    updateNombre(e) {
        this.nombre = e.target.value;
    }
    updateApellidos(e) {
        this.apellidos = e.target.value;
    }
    updateAnyosAntiguedad(e) {
        this.anyosAntiguedad = e.target.value;
    }

    origenChange(e) {
        var origen = e.detail.message;
        if (origen === "USA") {
            this.bg = "pink";
        }
        else if (origen === "México") {
            this.bg = "lightgreen";
        } else if (origen === "Canadá") {
            this.bg = "lightyellow";
        }
    }
}

customElements.define('ficha-persona', FichaPersona);